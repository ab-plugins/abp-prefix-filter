<?php
/**
 * Adds a prefix filter in display forum page
 * Copyright 2020 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}
define('CN_ABPPFILT', str_replace('.php', '', basename(__FILE__)));

/**
 * Informations about the plugin
 * @global MyLanguage $lang
 * @return array
 */
function abp_prefix_filter_info() {
    global $lang;
    $lang->load(CN_ABPPFILT);
    return array(
        'name' => $lang->abp_pfilter_name,
        'description' => $lang->abp_pfilter_desc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-prefix-filter',
        'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '1.2',
        'compatibility' => '18*',
        'codename' => CN_ABPPFILT
    );
}
